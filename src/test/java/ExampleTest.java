import java.time.Duration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ExampleTest {

  private WebDriver driver;

  private WebDriverWait wait;


  @BeforeEach
  public void setup() {
    this.driver = new ChromeDriver();
    final var duration = Duration.ofSeconds(10);
    this.wait = new WebDriverWait(driver, duration);
  }

  @Test
  public void mustFillContactForm() {
    driver.get("https://newm.com.br");

    goToContactPage();
    fillContactForm();
    pressSendButton();
    assertFormFeedback();
  }

  private void goToContactPage() {
    final var contactLink = findElementByCss("a[aria-label=\"Contato\"]");
    contactLink.click();
  }

  private void fillContactForm() {
    final var nameField = findElementByCss("input[placeholder=\"nome completo\"]");
    final var emailField = findElementByCss("input[placeholder=\"email\"]");

    nameField.sendKeys("Example Name");
    emailField.sendKeys("example@example.com");
  }

  private void pressSendButton() {
    final var button = findElementByCss("div.col-md-3.col-xl-2.mr-auto > div > button");

    try {
      Thread.sleep(3000);
    } catch (InterruptedException e) {
      throw new RuntimeException(e);
    }

    button.click();
  }

  private void assertFormFeedback() {
    findElementByCss("input[placeholder=\"telefone\"] + .invalid-feedback");
    findElementByCss("input[placeholder=\"assunto\"] + .invalid-feedback");
    findElementByCss("textarea[placeholder=\"mensagem\"] + .invalid-feedback");
  }

  private WebElement findElementByCss(String selector) {
    return wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(selector)));
  }
}
